var express = require("express");
var app = express();
var history = require("connect-history-api-fallback");

app.set("port", process.env.PORT || 5000);

app.use(history());
app.use(express.static("./build/"));

app.get("/", function(request, response) {
  response.render("./build/index.html");
});

app.listen(app.get("port"), function() {
  console.log("Node app is running on port", app.get("port"));
});
