import React, { Component } from "react";
import { HomeContainer, root_reducer } from "./components/home";
import { createStore, applyMiddleware, compose } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

let composer;
if (process.env.NODE_ENV === "development") {
  composer = compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
} else {
  composer = compose(applyMiddleware(thunk));
}
const store = createStore(root_reducer, composer);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <HomeContainer />
      </Provider>
    );
  }
}

export default App;
