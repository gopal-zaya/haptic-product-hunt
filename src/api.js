import axios from "axios";
export const PRODUCT_HUNT_API = axios.create({
  baseURL: "https://api.producthunt.com",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    Authorization: `Bearer ${process.env.REACT_APP_TOKEN}`
  }
});
