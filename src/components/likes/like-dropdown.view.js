import React from "react";
import { Popup, Button, Icon, Label } from "semantic-ui-react";
import LikeListContainer from "./like-list.container";

class LikeDropDown extends React.Component {
  render() {
    const { number_of_likes } = this.props;
    return (
      <Popup
        trigger={
          <Button size="mini">
            <Icon name="like" />
            {number_of_likes ? (
              <Label color="red" floating size="mini">
                {number_of_likes}
              </Label>
            ) : (
              false
            )}
            Show all
          </Button>
        }
        hoverable
        position="bottom right"
        style={{ maxHeight: "70vh", overflowY: "scroll" }}
        on="click"
      >
        <LikeListContainer />
      </Popup>
    );
  }
}

export default LikeDropDown;
