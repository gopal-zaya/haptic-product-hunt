import { connect } from "react-redux";
import Like from "./like.view";
import { put_like, remove_like } from "./actions";

const mapState = state => ({
  likes: Object.keys(state.likes).reduce((acc, key) => {
    acc = { ...acc, [key]: true };
    return acc;
  }, {})
});
const mapDispatch = dispatch => ({
  post_like: product => dispatch(put_like(product)),
  remove_like: id => dispatch(remove_like(id))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  post_like: () => {
    dispatchProps.post_like(ownProps.product);
  },
  remove_like: () => {
    dispatchProps.remove_like(ownProps.product.id);
  },
  can_like: !stateProps.likes.hasOwnProperty(ownProps.product.id)
});
const LikeContainer = connect(
  mapState,
  mapDispatch,
  mergeProps
)(Like);

export default LikeContainer;
