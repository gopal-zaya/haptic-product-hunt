import { PUT_LIKE, REMOVE_LIKE } from "./constants";

export const likes = (state = {}, action) => {
  switch (action.type) {
    case PUT_LIKE:
      return { ...state, [action.product.id]: action.product };
    case REMOVE_LIKE:
      return Object.keys(state).reduce((acc, key) => {
        if (key !== action.id.toString()) {
          acc = { ...acc, [key]: state[key] };
        }
        return acc;
      }, {});
    default:
      return state;
  }
};
