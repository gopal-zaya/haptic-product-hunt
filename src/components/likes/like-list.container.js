import { connect } from "react-redux";
import LikeListView from "./like-list.view";

const mapState = state => ({
  likes: state.likes
});
const LikeListContainer = connect(mapState)(LikeListView);
export default LikeListContainer;
