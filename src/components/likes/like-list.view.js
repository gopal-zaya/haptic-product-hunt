import React from "react";
import LikeListItem from "./like-list-item.view";
import { List } from "semantic-ui-react";

class LikeListView extends React.Component {
  render() {
    const { likes } = this.props;
    if (!Object.keys(likes).length)
      return <span>You've not liked any product yet.</span>;
    return (
      <List relaxed="very">
        {Object.values(likes).map((product, i) => (
          <LikeListItem key={i} product={product} />
        ))}
      </List>
    );
  }
}

export default LikeListView;
