import React from "react";
import { List, Grid, Image } from "semantic-ui-react";

class LikeListItem extends React.Component {
  render() {
    const { name, thumbnail, tagline } = this.props.product;
    return (
      <List.Item className="margin-sm">
        <List.Content>
          <Grid>
            <Grid.Column width={4} className="no-padding">
              <Image src={thumbnail} />
            </Grid.Column>
            <Grid.Column width={12} style={{ paddingTop: "0px" }}>
              <List.Header as="a">{name}</List.Header>
              <List.Description>{tagline}</List.Description>
            </Grid.Column>
          </Grid>
        </List.Content>
      </List.Item>
    );
  }
}

export default LikeListItem;
