import { connect } from "react-redux";
import LikeDropDown from "./like-dropdown.view";

const mapState = state => ({
  number_of_likes: Object.keys(state.likes).length
});
const LikeDropDownContainer = connect(mapState)(LikeDropDown);
export default LikeDropDownContainer;
