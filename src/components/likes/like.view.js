import React from "react";
import { Button, Icon } from "semantic-ui-react";

class Like extends React.Component {
  handleClick = e => {
    e.stopPropagation();
    const { remove_like, post_like, can_like } = this.props;
    if (can_like) {
      post_like();
    } else {
      remove_like();
    }
  };
  render() {
    const { can_like } = this.props;
    return (
      <Button
        size="mini"
        color={can_like ? "grey" : "red"}
        floated="right"
        onClick={this.handleClick}
      >
        <Icon name="like" /> Like
      </Button>
    );
  }
}

export default Like;
