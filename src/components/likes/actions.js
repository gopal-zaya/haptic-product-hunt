import { PUT_LIKE, REMOVE_LIKE } from "./constants";

export const put_like = product => {
  return { type: PUT_LIKE, product };
};
export const remove_like = id => {
  return { type: REMOVE_LIKE, id };
};
