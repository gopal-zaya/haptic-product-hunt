import React from "react";
import { Comment, Icon } from "semantic-ui-react";
import moment from "moment";
class Review extends React.Component {
  render() {
    const { user, body, created_at, votes } = this.props.review;
    return (
      <Comment>
        <Comment.Avatar as="a" src={user.image_url["60px"]} />
        <Comment.Content>
          <Comment.Author>{user.name}</Comment.Author>
          <Comment.Metadata>
            <div>{moment(created_at).fromNow()}</div>
            <div>
              <Icon name="star" />
              {votes} Vote{votes > 1 && "s"}
            </div>
          </Comment.Metadata>
          <Comment.Text>{body}</Comment.Text>
        </Comment.Content>
      </Comment>
    );
  }
}

export default Review;
