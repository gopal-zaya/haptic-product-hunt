export const SELECT_PRODUCT = "SELECT_PRODUCT";
export const FETCHING_COMMENTS = "FETCHING_COMMENTS";
export const SET_COMMENTS = "SET_COMMENTS";
export const COMMENTS_PER_PAGE = 5;
export const TOGGLE_MODAL = "TOGGLE_MODAL";
export const INIT_COMMENTS = "INIT_COMMENTS";
