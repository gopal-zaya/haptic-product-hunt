import React from "react";
import { Comment, Header } from "semantic-ui-react";
import Review from "./product-review.view";

class ReviewListView extends React.Component {
  render() {
    const { reviews, total_review_count } = this.props;
    if (!total_review_count) return <span>No reviews given.</span>;
    if (!reviews.length) return false;
    return (
      <React.Fragment>
        <Header as="h4">
          Showing {reviews.length} of {total_review_count} review
          {total_review_count > 1 && "s"}
        </Header>
        <Comment.Group>
          {reviews.map((review, i) => (
            <Review key={i} review={review} />
          ))}
        </Comment.Group>
      </React.Fragment>
    );
  }
}

export default ReviewListView;
