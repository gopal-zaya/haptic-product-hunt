import {
  SELECT_PRODUCT,
  COMMENTS_PER_PAGE,
  SET_COMMENTS,
  TOGGLE_MODAL,
  INIT_COMMENTS,
  FETCHING_COMMENTS
} from "./constants";
import { PRODUCT_HUNT_API } from "../../api";

export const toggle_modal = () => {
  return { type: TOGGLE_MODAL };
};
export const select_product = product => {
  return dispatch => {
    dispatch(set_product(product));
    dispatch(init_comments());
  };
};
const set_product = product => {
  return { type: SELECT_PRODUCT, product };
};
const init_comments = () => {
  return { type: INIT_COMMENTS };
};
const set_comments = (comments, next_page) => {
  return { type: SET_COMMENTS, comments, next_page };
};
const fetching_comments = () => {
  return { type: FETCHING_COMMENTS };
};
export const fetch_comments = (post_id, next_page) => {
  return dispatch => {
    dispatch(fetching_comments());
    return PRODUCT_HUNT_API.get("/v1/comments", {
      params: {
        "search[post_id]": post_id,
        per_page: COMMENTS_PER_PAGE,
        page: next_page
      }
    })
      .then(response => {
        dispatch(set_comments(response.data.comments, next_page));
      })
      .catch(error => console.log(error));
  };
};
