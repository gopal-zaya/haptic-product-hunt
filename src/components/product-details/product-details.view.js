import React from "react";
import { Modal, Image, Header, Button } from "semantic-ui-react";
import ReviewListContainer from "./product-review-list.container";

class ProductDetailsView extends React.Component {
  componentDidMount() {
    const { current_page } = this.props;
    if (current_page === 0) {
      this.handleFetchComments();
    }
  }
  handleFetchComments = () => {
    const { current_page, fetch_comments, product } = this.props;
    const next_page = current_page + 1;
    fetch_comments(product.id, next_page);
  };
  render() {
    const {
      product,
      toggle_modal,
      modal_open,
      fetching_comments,
      can_fetch_comments
    } = this.props;
    return (
      <Modal
        onClose={toggle_modal}
        open={modal_open}
        closeIcon
        centered={false}
      >
        {product && (
          <React.Fragment>
            <Modal.Header>{product.name}</Modal.Header>
            <Modal.Content image>
              <Image wrapped size="medium" src={product.thumbnail.image_url} />
              <Modal.Description>
                <Header>About</Header>
                <p>{product.tagline}</p>
                <Header>User reviews</Header>
                <ReviewListContainer />
                {can_fetch_comments && (
                  <Button
                    basic
                    size="tiny"
                    onClick={this.handleFetchComments}
                    disabled={fetching_comments}
                  >
                    {fetching_comments ? "fetching reviews..." : "Show more +"}
                  </Button>
                )}
              </Modal.Description>
            </Modal.Content>
          </React.Fragment>
        )}
      </Modal>
    );
  }
}

export default ProductDetailsView;
