import { product_selected } from "./product-details.reducer";
import ProductDetailsContainer from "./product-details.container";

export { product_selected, ProductDetailsContainer };
