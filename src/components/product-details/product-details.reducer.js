import {
  SELECT_PRODUCT,
  FETCHING_COMMENTS,
  SET_COMMENTS,
  TOGGLE_MODAL,
  INIT_COMMENTS
} from "./constants";
const initial_state = {
  product: null,
  comments: { current_page: 0, list: [], fetching: false },
  modal_open: false
};
export const product_selected = (state = initial_state, action) => {
  switch (action.type) {
    case SELECT_PRODUCT:
      return {
        ...state,
        product: { ...action.product }
      };
    case INIT_COMMENTS:
      return {
        ...state,
        comments: { ...initial_state.comments }
      };
    case TOGGLE_MODAL:
      return { ...state, modal_open: !state.modal_open };
    case FETCHING_COMMENTS:
      return { ...state, comments: { ...state.comments, fetching: true } };
    case SET_COMMENTS:
      const list = state.comments.list;
      return {
        ...state,
        comments: {
          fetching: false,
          current_page: action.next_page,
          list: [...list, ...action.comments]
        }
      };
    default:
      return state;
  }
};
