import { connect } from "react-redux";
import ReviewListView from "./product-review-list.view";

const mapState = state => ({
  reviews: state.product_selected.comments.list,
  total_review_count: state.product_selected.product.comments_count
});
const ReviewListContainer = connect(mapState)(ReviewListView);
export default ReviewListContainer;
