import { connect } from "react-redux";
import { fetch_products } from "../product-list/actions";
import HomeView from "./home.view";

const mapState = state => ({
  modal_open: state.product_selected.modal_open
});
const mapDispatch = dispatch => ({
  fetch_products: date => dispatch(fetch_products(date))
});

const HomeContainer = connect(
  mapState,
  mapDispatch
)(HomeView);

export default HomeContainer;
