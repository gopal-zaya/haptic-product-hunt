import { combineReducers } from "redux";
import { product_list } from "../product-list";
import { product_selected } from "../product-details";
import { likes } from "../likes";
const root_reducer = combineReducers({
  product_list,
  product_selected,
  likes
});

export default root_reducer;
