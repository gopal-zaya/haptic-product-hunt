import React from "react";
import { Menu, Container } from "semantic-ui-react";
import { ProductListContainer, ProductFilterContainer } from "../product-list";
import { LikeDropDownContainer } from "../likes";
import { ProductDetailsContainer } from "../product-details";
import "semantic-ui-css/semantic.min.css";
import "../../styles/size-modifier.css";
import "../../styles/common.css";

const TopMenu = () => (
  <Menu fixed="top" inverted style={{ height: "65px" }}>
    <Container>
      <Menu.Item header>Product hunt</Menu.Item>
      <Menu.Menu position="right">
        <Menu.Item>
          <LikeDropDownContainer />
        </Menu.Item>
      </Menu.Menu>
    </Container>
  </Menu>
);

class HomeView extends React.Component {
  componentDidMount() {
    const { fetch_products } = this.props;
    fetch_products();
  }

  render() {
    const { modal_open } = this.props;
    return (
      <React.Fragment>
        {modal_open && <ProductDetailsContainer />}
        <TopMenu />
        <Container className="margin-top-xl">
          <ProductFilterContainer />
          <ProductListContainer />
        </Container>
      </React.Fragment>
    );
  }
}

export default HomeView;
