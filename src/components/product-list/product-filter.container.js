import { connect } from "react-redux";
import { fetch_products } from "./actions";
import ProductFilter from "./product-filter.view";

const mapState = state => ({
  date: state.product_list.date,
  fetching: state.product_list.fetching
});

const mapDispatch = dispatch => ({
  fetch_products: date => dispatch(fetch_products(date))
});

const ProductFilterContainer = connect(
  mapState,
  mapDispatch
)(ProductFilter);

export default ProductFilterContainer;
