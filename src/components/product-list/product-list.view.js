import React from "react";
import { Card } from "semantic-ui-react";
import ProductCardContainer from "./product-card.container";

class ProductListView extends React.Component {
  render() {
    const { products, fetching } = this.props;
    if (fetching) return false;
    if (!products.length) return <span>No results found</span>;
    return (
      <Card.Group itemsPerRow={4} stackable className="margin-top-md">
        {products.map((product, i) => {
          return <ProductCardContainer key={i} product={product} />;
        })}
      </Card.Group>
    );
  }
}

export default ProductListView;
