import { connect } from "react-redux";
import ProductDetailsView from "../product-details/product-details.view";
import { fetch_comments, toggle_modal } from "../product-list/actions";

const mapState = state => ({
  product: state.product_selected.product,
  current_page: state.product_selected.comments.current_page,
  modal_open: state.product_selected.modal_open,
  fetching_comments: state.product_selected.comments.fetching,
  fetched_review_count: state.product_selected.comments.list.length
});

const mapDispatch = dispatch => ({
  toggle_modal: () => dispatch(toggle_modal()),
  fetch_comments: (post_id, next_page) =>
    dispatch(fetch_comments(post_id, next_page))
});

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  can_fetch_comments:
    stateProps.fetched_review_count < stateProps.product.comments_count
});

const ProductDetailsContainer = connect(
  mapState,
  mapDispatch,
  mergeProps
)(ProductDetailsView);

export default ProductDetailsContainer;
