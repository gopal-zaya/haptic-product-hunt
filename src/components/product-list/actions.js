import { SET_PRODUCTS, FETCHING_PRODUCTS, SET_DATE } from "./constants";
import { PRODUCT_HUNT_API } from "../../api";

const fetching_products = () => {
  return { type: FETCHING_PRODUCTS };
};
const set_products = products => {
  return { type: SET_PRODUCTS, products };
};

export const set_date = date => {
  return { type: SET_DATE, date };
};
export const fetch_products = date => {
  return dispatch => {
    dispatch(fetching_products());
    return PRODUCT_HUNT_API.get(
      "/v1/posts",
      date && {
        params: {
          day: date
        }
      }
    )
      .then(response => {
        dispatch(set_date(date));
        dispatch(set_products(response.data.posts));
      })
      .catch(error => {
        console.error(error);
      });
  };
};
