import { SET_PRODUCTS, FETCHING_PRODUCTS, SET_DATE } from "./constants";
import moment from "moment";

export const product_list = (
  state = {
    fetching: false,
    products: [],
    date: moment().format("YYYY-MM-DD")
  },
  action
) => {
  switch (action.type) {
    case FETCHING_PRODUCTS:
      return { ...state, fetching: true, products: [] };
    case SET_DATE:
      return { ...state, date: action.date };
    case SET_PRODUCTS:
      return { ...state, products: [...action.products], fetching: false };
    default:
      return state;
  }
};
