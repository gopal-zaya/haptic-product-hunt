import { connect } from "react-redux";
import { select_product, toggle_modal } from "../product-details/actions";
import ProductCardView from "./product-card.view";
import get from "lodash/get";

const mapDispatch = dispatch => ({
  select_product: product => dispatch(select_product(product)),
  toggle_modal: () => dispatch(toggle_modal())
});

const mapState = state => ({
  selected_product_id: get(state, "product_selected.product.id", null)
});
const mergeProps = (stateProps, dispatchProps, OwnProps) => ({
  ...OwnProps,
  ...stateProps,
  select_product: () => {
    dispatchProps.toggle_modal();
    if (
      !stateProps.selected_product_id ||
      stateProps.selected_product_id !== OwnProps.product.id
    ) {
      dispatchProps.select_product(OwnProps.product);
    }
  }
});

const ProductCardContainer = connect(
  mapState,
  mapDispatch,
  mergeProps
)(ProductCardView);

export default ProductCardContainer;
