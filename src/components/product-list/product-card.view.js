import React from "react";
import { Image, Card, Icon, Label } from "semantic-ui-react";
import { LikeContainer } from "../likes";

class ProductCardView extends React.Component {
  render() {
    const {
      tagline,
      thumbnail,
      votes_count,
      comments_count,
      topics,
      name,
      id
    } = this.props.product;
    const { select_product } = this.props;
    const like_product_props = {
      name,
      tagline,
      id,
      thumbnail: thumbnail.image_url
    };
    const topicsString = topics.map(topic => topic.name).join(", ");
    return (
      <Card onClick={select_product}>
        <Image src={thumbnail.image_url} centered fluid />
        <Card.Content>
          <Card.Header>{name}</Card.Header>
          <Card.Meta>
            <span className="date">{topicsString}</span>
          </Card.Meta>
          <Card.Description>{tagline}</Card.Description>
        </Card.Content>
        <Card.Content extra>
          <LikeContainer product={like_product_props} />
          <Label.Group circular size="tiny">
            <Label>
              <Icon name="comments" /> {comments_count}
            </Label>
            <Label>
              <Icon name="like" /> {votes_count}
            </Label>
          </Label.Group>
        </Card.Content>
      </Card>
    );
  }
}

export default ProductCardView;
