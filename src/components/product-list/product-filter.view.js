import React from "react";
import { Button, Segment, Header, Input } from "semantic-ui-react";
import moment from "moment";

class ProductFilter extends React.Component {
  handleSearch = () => {
    if (this.date) {
      this.props.fetch_products(this.date);
    }
  };
  handleChange = e => {
    this.date = e.target.value;
  };
  render() {
    const { fetching, date } = this.props;
    return (
      <Segment clearing>
        <Header floated="left">
          {fetching
            ? `Fetching product...`
            : `Showing results from ${moment(date).fromNow()}`}
        </Header>
        <Header floated="right" as="h5">
          <label>Search by date : </label>
          <Input
            type="date"
            onChange={this.handleChange}
            action={
              <Button
                color="blue"
                disabled={fetching}
                onClick={this.handleSearch}
              >
                {fetching ? `Searching...` : `Search`}
              </Button>
            }
          />
        </Header>
      </Segment>
    );
  }
}

export default ProductFilter;
