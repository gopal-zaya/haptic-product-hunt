import { connect } from "react-redux";
import ProductListView from "./product-list.view";

const mapState = state => ({
  products: state.product_list.products,
  fetching: state.product_list.fetching
});

const ProductListContainer = connect(mapState)(ProductListView);

export default ProductListContainer;
